createSealedSecrets:
	cat helm/secrets/prod/pullSecret.yaml | kubeseal > helm/secrets/prod/sealedPullSecret.yaml
	cat helm/secrets/stage/pullSecret.yaml | kubeseal > helm/secrets/stage/sealedPullSecret.yaml
	cat helm/secrets/stage/basicAuth.yaml | kubeseal > helm/secrets/stage/sealedBasicAuth.yaml
