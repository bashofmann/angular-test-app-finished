describe('Product List', function() {
  it('successfully loads', function() {
    cy.visit('/', {
      auth: {
        username: 'foo',
        password: 'bar',
      },
    });

    cy.contains('Products')
  });
});
